<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div>
            <h1>
                FUNCIONES
            </h1>
        </div>
        <div>
            <ul>
                <li>
                    <a href="funciones/funcion.php">
                        Crear una funcion
                    </a>
                </li>
                <li>
                    <a href="funciones/retorno.php">
                        Funcion que recibe argumentos y devuelve un valor
                    </a>
                </li>
                <li>
                    <a href="funciones/devolver.php">
                        Funcion que recibe argumentos y devuelve mas de un valor utilizando arrays
                    </a>
                </li>
                                <li>
                    <a href="funciones/devolver1.php">
                        Funcion que recibe argumentos y devuelve mas de un valor utilizando arrays
                    </a>
                </li>
                <li>
                    <a href="funciones/funcionargumentosVariables.php">
                        Pasar argumentos variables a una funcion metodo antiguo
                    </a>
                </li>
                <li>
                    <a href="funciones/argumentosVariables.php">
                        Pasar argumentos variables a una funcion metodo nuevo
                    </a>
                </li>
                                <li>
                    <a href="funciones/argumentosVariables1.php">
                        Pasar argumentos variables a una funcion metodo antiguo
                        otro ejemplo
                    </a>
                </li>
                                                <li>
                    <a href="funciones/argumentosVariables2.php">
                        Pasar argumentos variables a una funcion metodo nuevo
                        otro ejemplo
                    </a>
                </li>
                <li>
                    <a href="funciones/argumentosVariables2.php">
                        Pasar varios argumentos a una funcion con un array
                    </a>
                </li>                
                <li>
                    <a href="funciones/menu.php">
                        Creacion de un menu pasado como array
                    </a>
                </li>                
                <li>
                    <a href="funciones/menu_1.php">
                        Creacion de varios menus pasados como array
                    </a>
                </li>                
                                <li>
                    <a href="funciones/ambito.php">
                        Variables locales y globales
                    </a>
                </li>    
                <li>
                    <a href="funciones/ambito1.php">
                        Variables locales y globales
                    </a>
                </li>    
                <li>
                    <a href="funciones/globales.php">
                        Variables locales y globales
                    </a>
                </li>    
                <li>
                    <a href="funciones/porValor.php">
                        Paso de parametros por Valor
                    </a>
                </li>    
                <li>
                    <a href="funciones/porReferencia.php">
                        Paso de parametros por Referencia
                    </a>
                </li>    
                <li>
                    <a href="funciones/sumaElementos.php">
                        Sumar los elementos de un array y devolverlos por referencia
                    </a>
                </li>    
                <li>
                    <a href="funciones/operacionesElementos.php">
                        Sumar, multiplicar y calcular la media de los elementos de un array y devolverlos por referencia
                    </a>
                </li>  
                <li>
                    <a href="funciones/sumaElementos.php">
                        Variables estaticas
                    </a>
                </li>    
            </ul>
        </div>
    </body>
</html>

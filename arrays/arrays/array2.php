<?php
require 'mostrar.php';

/**
 * Crear e inicializar el array
 */

// arrays enumerado
$v3[10]=10; // a lo bestia
$v3['nombre']=15;
$v4=array(1,3,"a","casa");
$v5=["ejemplo de clase",12,true];

//arrays asociativos
$v6=array(
    "va1"=>10,
    "va2"=>"ww",
    "va3"=>13
);

$v7=[
    "va1"=>10,
    "va2"=>"ww",
    "va3"=>13
];

  /**
  * Arrays bidimensionales
  */
 
 $v8=[
     0=>["a","b"],
     "vv1"=>[1,2,3,4],
 ];
 
 
 /**
  * arrays multidimensionales
  */
 
 $v9=[
     "tabla1"=>[
         "tabla11"=>12,
         1,
         2,
         3
     ],
     "tabla2"=>[
         "tabla21"=>[1,2,3,4],
         "tabla22"=>23
     ]
 ];

 
 // crear un array con variables (al reves que extract)
 $indice1="valor1";
 $indice2="valor2";
 $indices=["indice1","indice2"];
 
 $array_compact= compact("indice1","indice2"); //crea un array cuyos indices son los nombres de las variables y los valores son los valores del array
 $array_compact1=compact($indices); // crea un array cuyos indices son los valores del array indices y los valores los valores de las variables que se llaman como los indices del array indices
 
 mostrarTodo(get_defined_vars());
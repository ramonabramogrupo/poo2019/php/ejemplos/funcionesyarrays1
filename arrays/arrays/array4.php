<?php
require 'mostrar.php';

// funciones para recorrer un array
$frutas = [
    'fruta1' => 'manzana',
    'fruta2' => 'naranja',
    'fruta3' => 'uva',
    'fruta4' => 'manzana',
    'fruta5' => 'manzana'
    ];

// leo el array hacia adelante
echo "<br>frutas<br>";
reset($frutas); // con la funcion reset coloco el puntero al principio del array
while ($nombre_fruta = current($frutas)) { // current lee el valor de la posicion del array
    if ($nombre_fruta == 'manzana') {
        echo key($frutas).'<br />'; // key lee la clave de la posicion del array
    }
    next($frutas); // apunta al siguiente elemento del array
}
echo "<br>frutas<br>";

// leo el array hacia atras
echo "<br>frutas<br>";
end($frutas); // end te coloca al final del array
while ($nombre_fruta = current($frutas)) { // ent lee el valor de la posicion del array
    if ($nombre_fruta == 'manzana') {
        echo key($frutas).'<br />'; // key lee la clave de la posicion del array
    }
    prev($frutas); // apunta al siguiente elemento del array
}
echo "<br>frutas<br>";

// la funcion each permite leer la clave y el valor

reset($frutas);
$posicion_frutas=each($frutas);

// vamos a recorrer el array con las funciones each y list
echo "<br>frutas<br>";
reset($frutas);
while (list($clave, $valor) = each($frutas)) {
    echo "$clave => $valor\n";
}
echo "<br>frutas<br>";

// vamos a recorrer el array pero intercambiando los indices por los valores
echo "<br>frutas<br>";
reset($frutas);
while (list($clave, $valor) = each($frutas)) {
    echo "$clave => $valor\n";
}
echo "<br>frutas<br>";

mostrarTodo(get_defined_vars());
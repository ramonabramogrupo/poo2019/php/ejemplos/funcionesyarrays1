<?php
require 'mostrar.php';
/**
  * Añadir elementos a una array
  */
 
 $inicial=[];
 $inicial[12]=1; // añadir en una posicion
 $inicial[]=10; // añadir en la siguiente posicion
 
 array_push($inicial, 1,2,3,4,5,6); //con esta funcion podemos añadir varios elementos
 
 $final[]=10; // si el array no existe le crea
 //array_push($aviso,1,2,3,4); // si el array no existe da error
 
 array_unshift($final, 0,0,0,0); // añade elementos al principio del array
 
 
 $auto1=array_fill(5, 10, 111); //crea un array enumerado con un indice inicial y con n elementos
 $auto2=range(0,100,2); // crea un array enumerado con elementos desde un valor inicial hasta un valor final con un paso dado
 $auto3=range('a','z',2); 
 
 $auto4= array_fill_keys($auto3, "ejemplo"); //crea un array con valores utilizando un array pasado como indices
 $auto5= array_fill_keys($auto2, 0);
 
 $auto7=array_pad([1,2],10,'a'); // añade elementos al final de un array
 $auto8=array_pad([1,2],-10,'casa'); // añade elementos al principio del array


mostrarTodo(get_defined_vars());
<?php
require 'mostrar.php';
// arrays enumerado
$v3[10]=10; // a lo bestia
$v4=array(1,3,"a","casa");
$v5=["ejemplo de clase",12,true];

//arrays asociativos
$v6=array(
    "va1"=>10,
    "va2"=>"ww",
    "va3"=>13
);

$v7=[
    "va1"=>10,
    "va2"=>"ww",
    "va3"=>13
];

 /**
  * Arrays bidimensionales
  */
 
 $v8=[
     0=>["a","b"],
     "vv1"=>[1,2,3,4],
 ];
 
 
 /**
  * arrays multidimensionales
  */
 
 $v9=[
     "tabla1"=>[
         "tabla11"=>12,
         1,
         2,
         3
     ],
     "tabla2"=>[
         "tabla21"=>[1,2,3,4],
         "tabla22"=>23
     ]
 ];


/**
 * crear variables con array
 */                         
                                                     
 list($a,$b)=$v4; //solo para arrays enumerados
 extract($v6);

  
 /**
  * Longitud del array
  */
 
 echo "<br>";
 echo count($v9);
 echo "<br>";
 echo sizeof($v8); //es una alias de count
 echo "<br>";
 echo count($v9,1); //cuenta de forma recursiva
 echo "<br>";


mostrarTodo(get_defined_vars());
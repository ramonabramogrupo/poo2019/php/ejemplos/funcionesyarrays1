<?php

/**
 * Crear array
 */

$v1=array(); //forma antigua
$v2=[]; //metodo nuevo

/**
 * Crear e inicializar el array
 */

// arrays enumerado
$v3[10]=10; // a lo bestia
$v4=array(1,3,"a","casa");
$v5=["ejemplo de clase",12,true];

//arrays asociativos
$v6=array(
    "va1"=>10,
    "va2"=>"ww",
    "va3"=>13
);

$v7=[
    "va1"=>10,
    "va2"=>"ww",
    "va3"=>13
];

  /**
  * Arrays bidimensionales
  */
 
 $v8=[
     0=>["a","b"],
     "vv1"=>[1,2,3,4],
 ];
 
 
 /**
  * arrays multidimensionales
  */
 
 $v9=[
     "tabla1"=>[
         "tabla11"=>12,
         1,
         2,
         3
     ],
     "tabla2"=>[
         "tabla21"=>[1,2,3,4],
         "tabla22"=>23
     ]
 ];

 
 // crear un array con variables (al reves que extract)
 $indice1="valor1";
 $indice2="valor2";
 $indices=["indice1","indice2"];
 
 $array_compact= compact("indice1","indice2"); //crea un array cuyos indices son los nombres de las variables y los valores son los valores del array
 $array_compact1=compact($indices); // crea un array cuyos indices son los valores del array indices y los valores los valores de las variables que se llaman como los indices del array indices
 
 
 
/**
 * Leer un array
 */

// un solo valor
echo "<br>";
echo '$v7["va1"]=>' . $v7['va1'];
echo "<br>";

//todos los valores
echo "<br>";
foreach($v6 as $i=>$v){
    echo "<br>$i=>$v";
}
echo "<br>";


// todos los valores en un array bidimensional
echo "<br>";
foreach($v8 as $i=>$v){
    echo "<br>";
    if(is_array($v)){
        echo "array " . $i; 
        foreach($v as $i1=>$v1){
            echo "<br>$i1=>$v1";
        }
    }else{
            echo "<br>$i=>$v";
    }
    
}
echo "<br>";

// los valores de un indice

$baseDatos=[
  [
      'nombre'=>'ramon',
      'edad'=>50,
      'email'=>'r@r.es'
  ],
  [
      'nombre'=>'rosa',
      'edad'=>23,
      'email'=>'r@r.es'
  ],
  [
      'nombre'=>'jose',
      'edad'=>32,
      'email'=>'r@r.es'
  ]  
];

$nombres=array_column($baseDatos, "nombre"); //crea un array con los nombres


// funciones para recorrer un array
$frutas = [
    'fruta1' => 'manzana',
    'fruta2' => 'naranja',
    'fruta3' => 'uva',
    'fruta4' => 'manzana',
    'fruta5' => 'manzana'
    ];

// leo el array hacia adelante
echo "<br>frutas<br>";
reset($frutas); // con la funcion reset coloco el puntero al principio del array
while ($nombre_fruta = current($frutas)) { // current lee el valor de la posicion del array
    if ($nombre_fruta == 'manzana') {
        echo key($frutas).'<br />'; // key lee la clave de la posicion del array
    }
    next($frutas); // apunta al siguiente elemento del array
}
echo "<br>frutas<br>";

// leo el array hacia atras
echo "<br>frutas<br>";
end($frutas); // end te coloca al final del array
while ($nombre_fruta = current($frutas)) { // ent lee el valor de la posicion del array
    if ($nombre_fruta == 'manzana') {
        echo key($frutas).'<br />'; // key lee la clave de la posicion del array
    }
    prev($frutas); // apunta al siguiente elemento del array
}
echo "<br>frutas<br>";

// la funcion each permite leer la clave y el valor

reset($frutas);
$posicion_frutas=each($frutas);

// vamos a recorrer el array con las funciones each y list
echo "<br>frutas<br>";
reset($frutas);
while (list($clave, $valor) = each($frutas)) {
    echo "$clave => $valor\n";
}
echo "<br>frutas<br>";

// vamos a recorrer el array pero intercambiando los indices por los valores
echo "<br>frutas<br>";
reset($frutas);
while (list($clave, $valor) = each($frutas)) {
    echo "$clave => $valor\n";
}
echo "<br>frutas<br>";

/**
 * crear variables con array
 */                         
                                                     
 list($a,$b)=$v4; //solo para arrays enumerados
 extract($v6);

  
 /**
  * Longitud del array
  */
 
 echo "<br>";
 echo count($v9);
 echo "<br>";
 echo sizeof($v8); //es una alias de count
 echo "<br>";
 echo count($v9,1); //cuenta de forma recursiva
 echo "<br>";
 
 
 /**
  * Añadir elementos a una array
  */
 
 $inicial=[];
 $inicial[12]=1; // añadir en una posicion
 $inicial[]=10; // añadir en la siguiente posicion
 
 array_push($inicial, 1,2,3,4,5,6); //con esta funcion podemos añadir varios elementos
 
 $final[]=10; // si el array no existe le crea
 array_push($aviso,1,2,3,4); // si el array no existe da error
 
 array_unshift($final, 0,0,0,0); // añade elementos al principio del array
 
 
 $auto1=array_fill(5, 10, 111); //crea un array enumerado con un indice inicial y con n elementos
 $auto2=range(0,100,2); // crea un array enumerado con elementos desde un valor inicial hasta un valor final con un paso dado
 $auto3=range('a','z',2); 
 
 $auto4= array_fill_keys($auto3, "ejemplo"); //crea un array con valores utilizando un array pasado como indices
 $auto5= array_fill_keys($auto2, 0);
 
 $auto7=array_pad([1,2],10,'a'); // añade elementos al final de un array
 $auto8=array_pad([1,2],-10,'casa'); // añade elementos al principio del array
 
 
 
 /**
  * Eliminar los elementos de un array
  */
 
 $borrar=[1,2,3,4,5,6,7,8];
 $borrar1=array_pop($borrar); // borra un elemento del final del array
 $borrar2= array_shift($borrar); // borra un elemento del principio del array
 $borrarA=[1,2,3,4,5,6,7];
 $borrarB=[
     'uno'=>10,
     'dos'=>20,
     'tres'=>30,
     'cuatro'=>40
 ];
 
 // elimina un elemento del array manteniendo los indices
 unset($borrarA[2]);
 unset($borrarB['dos']);
 
 // elimina elementos de un array perdiendo los indices
 
  $borrarA1=[1,2,3,4,5,6,7];
 $borrarB1=[
     'uno'=>10,
     'dos'=>20,
     'tres'=>30,
     'cuatro'=>40
 ];
 array_splice($borrarA1,3,2);
 array_splice($borrarB1,2,1); //puede borrar array asociativos
 
 
  
 // funcion de filtrado de elementos
 $borrar3= array_filter($borrar,function($v){
                                                if($v%2){
                                                    return TRUE;
                                                } else {
                                                    RETURN FALSE;
                                                }
                                            }); // elimina los elementos del array si la funcion devuelve false
 $borrar4=array_filter([
                            'uno'=>1,
                            'dos'=>2,
                            'tres'=>3
                        ],function($v,$i){
                                           if(strlen($i)<4 && $v<2){
                                               return TRUE;
                                           }else{
                                               return FALSE;
                                           }
                                        },ARRAY_FILTER_USE_BOTH); // podemos pasar la clave y el valor a array_filter
 $minimo=5;
 $borrar5=array_filter($borrar,function($v)use($minimo){
                                                          if($v>=$minimo){
                                                              return TRUE;
                                                          }  
                                                        }); //podemos utilizar la instruccion use para pasar un argumento a la funcion anonima
 

  // elimina elementos repetidos
                                                        
  $repetidos=[1,"uno"=>1,"dos"=>2,1,2,3,1,4,2,2,2,3,4,5,6,7];
  
  $sinRepetidos= array_unique($repetidos);
 
 /**
  * Operadores de un array
  */
 
 // unir
 $o1=[1,2,3];
 $o2=[4,5,6,7];
 
 $o3=$o1+$o2;
 $o4=$o2+$o1; // no es conmutativa
 
 $o11=[
     'a'=>10,
     'b'=>11,
     12
 ];

 $o12=[
     21,
     'a'=>22
 ];
 
 $o13=$o11+$o12;
 $o14=$o12+$o11; // tiene prioridad el de la izquierda
 
// comparar

$o20=[1,2,3];
$o21=[1,2,3];
$o22=[2,1,3];
$o23=[0=>1,2=>3,1=>2];

echo "<br>";
echo '$o20==$o21==>' . (int)($o20==$o21);
echo "<br>";
echo '$o20==$o22==>' . (int)($o20==$o22);
echo "<br>";
echo '$o23==$o20==>' . (int)($o23==$o20);
echo "<br>";
echo '$o23===$o20==>' . (int)($o23===$o20); // comprueba que esten en el mismo orden
echo "<br>";
 

  /**
  * Unir arrays
  */
 
 // combina dos arrays uno hace de claves y otro de valores
 $claves=['nombre','edad','tel'];
 $valores=['Ramon',44,'987987987'];
 $auto6= array_combine($claves, $valores);
 
 // combina arrays
 
 $array1    = array("color" => "red", 2, 4);
 $array2    = array("a", "b", "color" => "green", "forma" => "trapezoide", 4);
 $resultado = array_merge($array1, $array2);
 
 // combina arrays con operador
 
 $resultado1=$array1+$array2;
 $resultado2=$array2+$array1; // no es conmutativa
 

 /**
  * ordenar arrays
  */
 $frutas1 = [
    'fruta1' => 'manzana',
    'fruta2' => 'naranja',
    'fruta3' => 'uva',
    'fruta4' => 'manzana',
    'fruta5' => 'manzana'
    ];
 
  $frutas2 = [
    'fruta1' => 'manzana',
    'fruta2' => 'naranja',
    'fruta3' => 'uva',
    'fruta4' => 'manzana',
    'fruta5' => 'manzana'
    ];
  $frutas3=$frutas1;
  $frutas4=$frutas1;
  $frutas5=$frutas1;
  $frutas6=$frutas1;
  
  
 // funcion que mantiene la posicion de los indices
 // Esta función se utiliza principalmente para ordenar arrays asociativos en los que el orden es importante.
    asort($frutas1); // orden ascendente
    arsort($frutas2); // orden descendente
 
// funcion que no mantiene los indices
    sort($frutas3); // orden ascendente
    rsort($frutas4); // orden descendente

// funcion que ordena por los indices (mantiene la coherencia entre los indices y los valores)
    ksort($frutas5);
    krsort($frutas6);

    
$frutas7 = [
    'fruta1' => 'manzana',
    'fruta2' => 'naranja',
    'fruta13' => 'uva',
    'fruta9' => 'manzana1',
    'fruta25' => 'manzana2'
    ];

$frutas8=$frutas7;
$frutas9=array_flip($frutas7);
$frutas10=$frutas9;
$frutas11=$frutas9;
$frutas12=$frutas9;

    
// Ordenes naturales

// orden por indices
   ksort($frutas7); // ordenacion normal
   ksort($frutas8,SORT_NATURAL); //ordenacion natural

// orden por valores
   asort($frutas9); // ordenacion normal
   natsort($frutas10); // ordenacion natural

// orden por valores sin coherencia de indices
   sort($frutas11); //ordenacion normal
   sort($frutas12,SORT_NATURAL); //ordenacion natural


   
 
 /**
  * Funciones especiales con arrays
  */

   /**
    * array_count_values
    */
   
   // contar repeticiones de un array (frecuencia)
   $contar=[
       'uno'=>1,
       1,20,3,4,1,1,14,1,1,1,
       'dos'=>2,
       'tres'=>3,
       2,2,2,2,2,2,2,10
   ];
   
   $contarValores= array_count_values($contar);
   
   /**
    * devolver valores
    */
   
   $todosValores= array_values($contarValores);
   
   
   /**
    * Devolver claves
    */
   
   $todosIndices= array_keys($contarValores);
   
   /**
    * Buscar valores
    */
   $buscar1=[1,2,3,1,2,3,12,3,12];
   
   $todosIndicesValor= array_keys($buscar1,2); // busca un valor y te indica en que indices lo ha encontrado
   $indiceValor= array_search(2,$buscar1); // buscar un valor y te indica el primer indice donde lo ha encontrado
 
   /**
    * Comprobar si existe una clave o valor
    */
   $buscar2=[
     'fruta1'=>'naranja',
     'fruta2'=>'manzana',
     'fruta3'=>'naranja'
   ];
   
   $existeValor= in_array('naranja',$buscar2);
   $existeIndice= array_key_exists('fruta1', $buscar2);
   
   /**
    * convierte un array en string
    */
   
  $unirCadena=[
  [
      'nombre'=>'ramon',
      'edad'=>50,
      'email'=>'r@r.es'
  ],
  [
      'nombre'=>'rosa',
      'edad'=>23,
      'email'=>'r@r.es'
  ],
  [
      'nombre'=>'jose',
      'edad'=>32,
      'email'=>'r@r.es'
  ]  
];
   
   $cadena=implode(",",$buscar2);
   //$cadena1=implode(",",$unirCadena); // no podemos utilizarlo con arrays bidimensionales
   
   /**
    * Selecciona elementos aleatorios de un array
    */
   
    $frutasAleatorio = [
    'fruta1' => 'manzana',
    'fruta2' => 'naranja',
    'fruta3' => 'uva',
    'fruta4' => 'manzana',
    'fruta5' => 'manzana'
    ];
   
    $aleatorio= array_rand($frutasAleatorio); // devuelve un indice de forma aleatoria
    $aleatorio2= array_rand($frutasAleatorio,3);
    $aleatorio3=array_rand($unirCadena); // un elemento de la primera dimension
    
    /**
     * Extrae un trozo del array
     */
    
    //indicamos los elementos a quedarnos
    $borrarA2=[1,2,3,4,5,6,7];
    $borrarB2=[
     'uno'=>10,
     'dos'=>20,
     'tres'=>30,
     'cuatro'=>40
    ];
 
    $borrarA21=array_slice($borrarA2, 3,2); //no mantiene los indices
    $borrarA22=array_slice($borrarA2, 3,2,TRUE); //mantiene los indices
    $borrarB21=array_slice($borrarB2,1,2); // mantiene los indices al ser asociativo
    
    /**
     * Aplica una operacion a los elementos de los arrays dados
     */
    
    $uno=[1,2,3,4,5];
    $dos=[10,10,10,20,30,40];
    $tres=[
    [
        'nombre'=>'ramon',
        'edad'=>50,
        'email'=>'r@r.es'
    ],
    [
        'nombre'=>'rosa',
        'edad'=>23,
        'email'=>'r@r.es'
    ],
    [
        'nombre'=>'jose',
        'edad'=>32,
        'email'=>'r@r.es'
    ]  
    ];
    $cinco=$tres;
    
    $cuatro=$uno;
    $seis=$tres;
    $walkSalida="";
    $salida="";
    
    $array_map=array_map(function($a,$b){return $a*$b;},$uno,$dos);
    
    /**
     * Aplica una funcion a los elementos del array pasado, permite pasar argumentos
     * No permite pasar argumentos por referencia
     * para ello tienes que utilizar use
     */
    
    array_walk($cuatro, function(&$v,$i,$a){$v=$a+$v+$i;},10);
    array_walk($tres,function(&$v,$i){$v=implode(",",$v);});
    array_walk($seis,function($v)use(&$salida){$salida.=implode(",",$v) .",";});

    // para arrays multidimensionales
    array_walk_recursive($cinco,function($v,$i){ 
        if(!is_array($v)){
            echo $v;
        }
    }); // no es necesario los valores que sean arrays no se pasan
    array_walk_recursive($cinco,function($v,$i){echo $v;}); // hace lo mismo que la anterior
    array_walk_recursive($cinco,function($v,$i)use(&$walkSalida){ $walkSalida.=$v . ",";}); // hace lo mismo que la anterior
    
    
    
    
 
/**
 * Depurar array
 */

var_dump($v3);
var_dump($v2);
var_dump($v1);
var_export($v5);
print_r($v4);
var_dump(get_defined_vars());
var_dump(array_filter(get_defined_vars(),function($i){
                                                        if($i[0]=="_") 
                                                            return FALSE;
                                                        else
                                                            return TRUE;
                                                     },ARRAY_FILTER_USE_KEY));
                                                 
                                                     
                                                     
                                                     
                                                     













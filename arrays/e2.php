<?php

$a=[
    'a'=>10,
    'b'=>20,
    'c'=>21,
    'd'=>32
];

var_dump(count($a));
var_dump(in_array(10, $a));
var_dump(array_key_exists('a', $a));
var_dump(array_keys($a));
var_dump(array_values($a));
var_dump(array_flip($a));
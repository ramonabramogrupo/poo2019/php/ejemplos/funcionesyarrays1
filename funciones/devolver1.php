<?php
   function sumar_restar($num1, $num2){
      $resultado = [
         'suma' => $num1 + $num2,
         'resta' => $num1 - $num2,
         'producto' => $num1*$num2,
         'cociente' => $num1/$num2,
      ];
      return $resultado;
   }
   
   $resultado = sumar_restar(20, 7);
   
   foreach ($resultado as $operacion => $valor) {
       echo "<div>$operacion: $valor</div>";
    }
   
?>        
    